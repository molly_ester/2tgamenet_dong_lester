using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class NetworkManager : MonoBehaviourPunCallbacks {

    [Header("Connected Status Panel")]
    public Text connectionStatusText;

    [Header("Login UI Panel")]
    public InputField playerNameInput;
    public GameObject loginUIPanel;
    
    [Header("Game Options Panel")]
    public GameObject gameOptionsPanel;

    [Header("Create Room Panel")]
    public GameObject createRoomPanel;
    public InputField roomNameInputField;
    public InputField playerCountInputField;

    [Header("Join Random Room Panel")]
    public GameObject joinRandomRoomPanel;

    [Header("Show Room List Panel")]
    public GameObject showRoomListPanel;

    [Header("Inside Room Panel")]
    public GameObject insideRoomPanel;
    public Text roomInfoText;
    public GameObject playerListItemPrefab;
    public GameObject playerListViewParent;
    public GameObject startGameButton;

    [Header("Room List Panel")]
    public GameObject roomListPanel;
    public GameObject roomItemPrefab;
    public GameObject roomListParent;

    Dictionary<string, RoomInfo> cachedRoomList;
    Dictionary<string, GameObject> roomListGameObject;
    Dictionary<int, GameObject> playerListGameObject;

    #region Unity Functions
    
    void Start(){
        cachedRoomList = new Dictionary<string, RoomInfo>();
        roomListGameObject = new Dictionary<string, GameObject>();
        ActivatePanel(loginUIPanel);

        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Update(){
        connectionStatusText.text = "Connection status: " + PhotonNetwork.NetworkClientState;
    }

    #endregion

    #region UI Callbacks
    public void OnLoginButtonClicked(){
        string playerName = playerNameInput.text;

        if(string.IsNullOrEmpty(playerName)){
            Debug.Log("Player name is Invalid");
        } 
        else {
            PhotonNetwork.LocalPlayer.NickName = playerName;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public void OnCreateRoomButtonClicked(){
        string roomName = roomNameInputField.text;

        if(string.IsNullOrEmpty(roomName)){
            roomName = "Room " + Random.Range(1000, 10000);
        }

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = (byte) int.Parse(playerCountInputField.text);
        PhotonNetwork.CreateRoom(roomName, roomOptions);
        
    }

    public void OnBackButtonClicked(){
        if(PhotonNetwork.InLobby){
            PhotonNetwork.LeaveLobby();
        }
        ActivatePanel(gameOptionsPanel);
    }

    public void OnCancelButtonClicked(){
        ActivatePanel(gameOptionsPanel);
    }

    public void OnShowRoomListButtonClicked(){
        if(!PhotonNetwork.InLobby){
            PhotonNetwork.JoinLobby();
        }
        ActivatePanel(showRoomListPanel);
    }

    public void OnLeaveGameButtonClicked(){
        PhotonNetwork.LeaveRoom();
    }

    public void OnJoinRandomRoomClicked(){
        ActivatePanel(joinRandomRoomPanel);
        PhotonNetwork.JoinRandomRoom(); 
    }
    
    public void OnStartGameButtonClicked(){
        PhotonNetwork.LoadLevel("GameScene");
    }
    #endregion

    #region PUN Callbacks
    public override void OnConnected(){
        Debug.Log("Connected to the Internet");
    }

    public override void OnConnectedToMaster(){
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " has connected to Photon Servers");
        ActivatePanel(gameOptionsPanel);
    }

    public override void OnCreatedRoom(){
        Debug.Log(PhotonNetwork.CurrentRoom.Name + " created!");
    }

    public override void OnJoinedRoom(){
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " has joined " + PhotonNetwork.CurrentRoom.Name);
        ActivatePanel(insideRoomPanel);
        roomInfoText.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + "Current Player Count: " + PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;

        if(playerListGameObject == null){
            playerListGameObject = new Dictionary<int, GameObject>();
        }
        foreach(Player player in PhotonNetwork.PlayerList){
            GameObject playerItem = Instantiate(playerListItemPrefab);
            playerItem.transform.SetParent(playerListViewParent.transform);
            playerItem.transform.localScale = Vector3.one;

            playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = player.NickName;
            playerItem.transform.Find("PlayerIndicator").gameObject.SetActive(player.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);

            playerListGameObject.Add(player.ActorNumber, playerItem);
        }
    }

    public override void OnPlayerEnteredRoom(Player player){
         roomInfoText.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + "Current Player Count: " + PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;
        GameObject playerItem = Instantiate(playerListItemPrefab);
        playerItem.transform.SetParent(playerListViewParent.transform);
        playerItem.transform.localScale = Vector3.one;

        playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = player.NickName;
        playerItem.transform.Find("PlayerIndicator").gameObject.SetActive(player.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);

        playerListGameObject.Add(player.ActorNumber, playerItem);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer){
        roomInfoText.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + "Current Player Count: " + PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;
        Destroy(playerListGameObject[otherPlayer.ActorNumber]);
        playerListGameObject.Remove(otherPlayer.ActorNumber);
        startGameButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);

    }

    public override void OnLeftRoom(){
        foreach(var gameObject in playerListGameObject.Values){
            Destroy(gameObject);
        }

        playerListGameObject.Clear();
        playerListGameObject = null;
        ActivatePanel(gameOptionsPanel);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList){
        ClearRoomListGameObjects();
        
        startGameButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);
        foreach(RoomInfo info in roomList){
            Debug.Log(info.Name);
            if(!info.IsOpen || !info.IsVisible || info.RemovedFromList) {
                if (cachedRoomList.ContainsKey(info.Name)){
                    cachedRoomList.Remove(info.Name);
                }
            }
            else {
                if(cachedRoomList.ContainsKey(info.Name)){
                    cachedRoomList[info.Name] = info;
                }
                else {
                    cachedRoomList.Add(info.Name, info);
                }  
            } 
        }

        foreach(RoomInfo info in cachedRoomList.Values) {
            GameObject listItem = Instantiate(roomItemPrefab);
            listItem.transform.SetParent(roomListParent.transform);
            listItem.transform.localScale = Vector3.one;

            listItem.transform.Find("RoomNameText").GetComponent<Text>().text = info.Name;
            listItem.transform.Find("RoomPlayersText").GetComponent<Text>().text = "Player count: " + info.PlayerCount + "/" + info.MaxPlayers;
            listItem.transform.Find("JoinRoomButton").GetComponent<Button>().onClick.AddListener(delegate{ OnJoinRoomClicked(info.Name);}); 

            roomListGameObject.Add(info.Name, listItem);
        }
    }

    public override void OnJoinRandomFailed(short returnCode, string message){
        Debug.LogWarning(message);
        string roomName = "Room " + Random.Range(1000, 10000);
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 20;

        PhotonNetwork.CreateRoom(roomName, roomOptions);


    }

    public override void OnLeftLobby(){
        ClearRoomListGameObjects();
        cachedRoomList.Clear();
    }

    #endregion 
    #region Private Methods
    void OnJoinRoomClicked(string roomName){
        if(PhotonNetwork.InLobby){
            PhotonNetwork.LeaveLobby();
        }
        PhotonNetwork.JoinRoom(roomName);
    } 

    private void ClearRoomListGameObjects(){
        foreach(var item in roomListGameObject.Values){
            Destroy(item);
        }

        roomListGameObject.Clear();
    }

    #endregion
    #region Public Methods
    public void ActivatePanel(GameObject panelToBeActivated) {
        loginUIPanel.SetActive(panelToBeActivated.Equals(loginUIPanel));
        gameOptionsPanel.SetActive(panelToBeActivated.Equals(gameOptionsPanel));
        createRoomPanel.SetActive(panelToBeActivated.Equals(createRoomPanel));
        joinRandomRoomPanel.SetActive(panelToBeActivated.Equals(joinRandomRoomPanel));
        showRoomListPanel.SetActive(panelToBeActivated.Equals(showRoomListPanel));
        insideRoomPanel.SetActive(panelToBeActivated.Equals(insideRoomPanel));
        roomListPanel.SetActive(panelToBeActivated.Equals(roomListPanel)); 
    }
    #endregion
}
