﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro; 

public class GameManager : MonoBehaviour
{
    static GameManager instance;
    public GameObject playerPrefab;
    public Transform[] spawnPoints;

    public GameObject KillerDeathPanelParent;
    public GameObject WinPanel;
    public static GameManager Instance => instance;
    public TextMeshProUGUI meshPro;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else instance = this;
    }


    void Start()
    {
        if(PhotonNetwork.IsConnectedAndReady){
            GameObject player = PhotonNetwork.Instantiate(playerPrefab.name, RandomSpawnPosition(), Quaternion.identity);
            player.GetComponent<PhotonView>().Owner.TagObject = player;
            //player.GetComponent<Shooting>().winner.AddListener((WinnerName) => photonView.RPC("SetActivePanel", RpcTarget.All, WinnerName));
            Debug.Log(player.GetComponent<PhotonView>().Owner.TagObject);
        }
    }

    public Vector3 RandomSpawnPosition()
    {
        float randomPointX = Random.Range(-1.0f, 1.0f);
        float randomPointZ = Random.Range(-1.0f, 1.0f);
        Vector3 position = spawnPoints[Random.Range(0, spawnPoints.Length - 1)].position + new Vector3(randomPointX, 0, randomPointZ);
        position = new Vector3(position.x, 3, position.z);
        return position;
    }

    public void GameFinish()
    {
        /*foreach (var players in PhotonNetwork.PlayerList)
        {
            GameObject obj = players.TagObject as GameObject;
            PlayerSetup player = obj.GetComponent<PlayerSetup>();
            player.PlayerUI.SetActive(false);
            
            
        }*/
        StartCoroutine(LeaveRoom());
    }

    IEnumerator LeaveRoom()
    {
        yield return new WaitForSeconds(.5f);
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel("LobbyScene");
    }
}
