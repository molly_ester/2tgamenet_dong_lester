﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public GameObject fpsModel;
    public GameObject nonFpsModel;

    public GameObject playerUiPrefab;

    public PlayerMovementController playerMovementController;
    public Camera fpsCamera;

    Animator animator;
    public Avatar fpsAvatar, nonFpsAvatar;
    public TextMeshProUGUI playerName;
    public Shooting shooting;
    GameObject playerUI;
    public GameObject PlayerUI => playerUI;

    // Start is called before the first frame update
    void Awake()
    {
        playerMovementController = GetComponent<PlayerMovementController>();
        animator = GetComponent<Animator>();
        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);
        animator.SetBool("isLocalPlayer", photonView.IsMine);
        animator.avatar = photonView.IsMine ? fpsAvatar : nonFpsAvatar;
        playerName.text = photonView.Owner.NickName;

        shooting = GetComponent<Shooting>();
        if (photonView.IsMine)
        {
            playerUI = Instantiate(playerUiPrefab);
            playerMovementController.fixedTouchField = playerUI.transform.Find("RotationTouchField").GetComponent<FixedTouchField>();
            playerMovementController.joystick = playerUI.transform.Find("Fixed Joystick").GetComponent<Joystick>();
            Debug.Log(playerUI);
            playerUI.transform.Find("FireButton").GetComponent<Button>().onClick.AddListener(() => shooting.Fire());
            playerName.text = photonView.Owner.NickName;
            fpsCamera.GetComponent<AudioListener>().enabled = true;
        }
        else
        {
            playerMovementController.enabled = false;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
            fpsCamera.enabled = false;
            

        }

        
    }
}
