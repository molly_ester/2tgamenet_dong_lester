﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.Events;
public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitParticlePrefab;

    [Header("HP Related")]
    public float startHealth = 100;
    float health;
    public Image healthBar;

    Animator animator;

    [Header("Kills")]
    public int killCount;
    Text killCountText;
    public GameObject killedWhoUIprefab;

    GameObject playerUI;

    

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = GetComponent<Animator>();

        if (photonView.IsMine)
        {
            playerUI = GetComponent<PlayerSetup>().PlayerUI;
            killCountText = playerUI.transform.Find("KillCounter").GetComponent<Text>();
            killCountText.text = "Kills: " + killCount;
        }
    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(.5f, .5f));
        if(Physics.Raycast(ray, out hit, 200)) {
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);
            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);   
            }
            
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;
        
        if(health <= 0)
        {
            //Die
            Die();
            Debug.Log(info.Sender.NickName + "Killed" + info.photonView.Owner.NickName);
            string name = info.Sender.NickName;
            if (!photonView.IsMine)
            {
                Debug.Log(info.Sender);
                GameObject killer = info.Sender.TagObject as GameObject;
                //Debug.Log(killer);
                Shooting killerShooter = killer.GetComponent<Shooting>();
                killerShooter.killCount++;
                killerShooter.killCountText.text = "Kills: " + killerShooter.killCount;
                photonView.RPC("ShowDeathUpdates", RpcTarget.All, info.Sender.NickName, info.photonView.Owner.NickName);
                if (killerShooter.killCount >= 10) {
                    photonView.RPC("SetActivePanel", RpcTarget.All, name);
                    GameManager.Instance.GameFinish();
                }            
            }

            
            //ShowDeathUpdates(info.Sender.NickName, info.photonView.Owner.NickName);
            /*foreach (var player in PhotonNetwork.PlayerList) {
                GameObject obj = player.TagObject as GameObject;
                Shooting shooting = obj.GetComponent<Shooting>();
                GameObject instancePrefab = Instantiate(killedWhoUIprefab);
                instancePrefab.transform.SetParent(shooting.playerUI.transform.Find("Player Death Panel"));
                instancePrefab.transform.Find("Killer").GetComponent<Text>().text = info.Sender.NickName;
                instancePrefab.transform.Find("Dead").GetComponent<Text>().text = info.photonView.Owner.NickName;

            }*/
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitParticlePrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        float respawnTime = 5.0f;
        GameObject respawnText = GameObject.Find("Respawn Text");
        transform.GetComponent<PlayerMovementController>().enabled = false;
        while (respawnTime > 0)
        {
            yield return new WaitForSeconds(1);
            respawnTime--;

            
            respawnText.GetComponent<Text>().text = "You are killed, Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        transform.position = GameManager.Instance.RandomSpawnPosition();
        transform.GetComponent<PlayerMovementController>().enabled = true;
        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }
    [PunRPC]
    public void RegainHealth()
    {
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void ShowDeathUpdates(string killer, string dead)
    {
        GameObject instancePrefab = Instantiate(killedWhoUIprefab);
        instancePrefab.transform.SetParent(GameManager.Instance.KillerDeathPanelParent.transform);
        instancePrefab.transform.Find("Killer").GetComponent<Text>().text = killer;
        instancePrefab.transform.Find("Dead").GetComponent<Text>().text = dead;

        Destroy(instancePrefab, 2f);
    }

    [PunRPC]
    public void SetActivePanel(string winnerName)
    {
        GameManager.Instance.WinPanel.SetActive(true);
        GameManager.Instance.meshPro.text = winnerName + " wins the Game!";
    }

}
