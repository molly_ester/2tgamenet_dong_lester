﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum RaiseEventCode {
    DestroyBlockEvent = 0,
    DeathPlayerEvent = 1,
    WinPlayerEvent = 2
}

public class GameManager : MonoBehaviourPunCallbacks {
    public Transform[] SpawnPositions;
    public GameObject[] Players;
    public static GameManager instance = null;
    public int AlivePlayers;
    public GameObject WinnerPanel;
    public Text winnerText;

    public GameObject EliminatedPanelPrefab;
    public GameObject EliminatedpanelParent;
    public Action Winner;

    void OnEnable() {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;

    }

    void OnDisable() {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent) {
        if (photonEvent.Code == (byte)RaiseEventCode.DeathPlayerEvent) {
            object[] data = (object[])photonEvent.CustomData;
            AlivePlayers = (int) data[0];
            if(AlivePlayers == 1) {
                Winner.Invoke();
            }

            GameObject x = Instantiate(EliminatedPanelPrefab, new Vector2(0, 0), Quaternion.identity);
            x.transform.SetParent(EliminatedpanelParent.transform);
            x.GetComponent<Text>().text = (string)data[1] + " is Eliminated";

        }
        if(photonEvent.Code == (byte)RaiseEventCode.WinPlayerEvent) {
            object[] data = (object[])photonEvent.CustomData;

            winnerText.text = (string)data[0] + " Wins";
            WinnerPanel.SetActive(true);

           // StartCoroutine(BackToLobby());
        }
    }
    private void Awake() {
        if (!instance) {
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject);
        }
    }

    void Start() {
        if (PhotonNetwork.IsConnectedAndReady) {
            object playerSelectionNumber;
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber)) {
                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = SpawnPositions[actorNumber - 1].position;
                GameObject x = PhotonNetwork.Instantiate(Players[(int)playerSelectionNumber - 1].name, instantiatePosition, Quaternion.identity);
                x.GetComponent<Health>().DeathEvt += PlayerDeath;
            }
        }

        if (PhotonNetwork.IsMasterClient) {
            photonView.RPC("SetPowerUp", RpcTarget.AllBuffered);
        }

        AlivePlayers = PhotonNetwork.CurrentRoom.PlayerCount;
    }

    public void PlayerDeath(GameObject obj) {
        obj.GetComponent<Health>().DeathEvt -= PlayerDeath;
        AlivePlayers--;

        object[] data = new object[] { AlivePlayers, obj.GetComponent<PhotonView>().Owner.NickName };
         RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
             Receivers = ReceiverGroup.All,
             CachingOption = EventCaching.AddToRoomCache
         };

        SendOptions options = new SendOptions {
            Reliability = false
        };
            PhotonNetwork.RaiseEvent((byte)RaiseEventCode.DeathPlayerEvent, data, raiseEventOptions, options);

            obj.GetComponent<Movement>().enabled = false;
            obj.GetComponent<BombDrop>().enabled = false;

            GameObject x = obj?.GetComponentInChildren<Camera>()?.gameObject;
            if (x) {
                x.AddComponent<Movement>().speed = 5;
                x.transform.SetParent(null);
            }

            obj.SetActive(false);
            //Destroy(obj);
        
    }
    
    IEnumerator BackToLobby() {
        yield return new WaitForSeconds(1);
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom() {
        SceneManager.LoadScene(0);
    }
}

//public GameObject[] vehiclePrefabs;
//public Transform[] startingPositions;

//public static RacingGameManager instance = null;

//public Text timeText;
//public GameObject[] finisherTextUI;
//public List<GameObject> lapTrigger = new List<GameObject>();

//private void Awake() {
//    if (!instance) {
//        instance = this;
//    }
//    else if (instance != this) {
//        Destroy(gameObject);
//    }
//}
//void Start() {
//    if (PhotonNetwork.IsConnectedAndReady) {
//        object playerSelectionNumber;
//        if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber)) {
//            Debug.Log((int)playerSelectionNumber);

//            int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
//            Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
//            PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
//        }
//    }

//    foreach (GameObject go in finisherTextUI) {
//        go.SetActive(false);
//    }
//}