using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUp : PowerUp
{
    protected override void Effect(GameObject boost) {
        base.Effect(boost);

        boost.GetComponent<Movement>().speed++;
    }
}
