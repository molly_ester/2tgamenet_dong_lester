using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombUp : PowerUp
{
    protected override void Effect(GameObject boost) {
        base.Effect(boost);
        boost.GetComponent<BombDrop>().bombToDrop++;
    }
}
