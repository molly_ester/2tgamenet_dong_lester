using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class PowerUp : MonoBehaviour {


    protected virtual void Effect(GameObject boost) {

    }

    void OnTriggerEnter2D(Collider2D collision) {

        if (collision.gameObject.CompareTag("Player")) {
            Effect(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
