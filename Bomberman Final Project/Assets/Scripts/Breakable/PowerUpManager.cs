using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PowerUpManager : MonoBehaviourPunCallbacks { 
    
    public Breakable[] breakables;

    public GameObject[] powerUpPrefabs;

    [PunRPC]
    public void SetPowerUp() {
        foreach (Breakable b in breakables) {
            int rand = Random.Range(0, 3);

            b.powerUpPrefab = rand == 0 ? powerUpPrefabs[Random.Range(0, powerUpPrefabs.Length)] : null;
        }
    }
}
