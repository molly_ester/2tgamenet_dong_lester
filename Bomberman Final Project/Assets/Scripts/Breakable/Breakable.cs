using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Breakable : MonoBehaviour {

    void OnEnable() {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    void OnDisable() {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent) {
        if (photonEvent.Code == (byte)RaiseEventCode.DestroyBlockEvent) {
            object[] data = (object[])photonEvent.CustomData;
            
            if(gameObject.name == (string) data[0])
                Destroy(gameObject);
        }
    }
    public GameObject powerUpPrefab;

    public void ToDestroy(string name) {
        object[] data = new object[] { name };
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions options = new SendOptions {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.DestroyBlockEvent, data, raiseEventOptions, options);
    }
}
