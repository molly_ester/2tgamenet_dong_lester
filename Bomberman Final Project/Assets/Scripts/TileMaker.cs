﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WallGeneration { 
    Random,
    Uniform
}


public class TileMaker : MonoBehaviour {
    
    [Header("Prefabs")]
    public GameObject FloorPrefab;
    //0 horizontal, 1 vertical
    public GameObject[] WallPrefab;

    [Header("Tiles To Generate")]
    public int Horizontal;
    public int Vertical;
    public int InnerWalls;
    public WallGeneration wallGen;


    GameObject instantiated;
    List<Vector2> spawnPoints = new List<Vector2>();
    /*private void OnValidate() {

        if (instantiated != null) {
            GameObject x = instantiated;
            UnityEditor.EditorApplication.delayCall += () =>
            {
                DestroyImmediate(x);
            };
        }
        instantiated = new GameObject("Tiles");

        GenerateFloors();
        GenerateWalls();
        GenerateInnerWalls();
    }*/

    public void GenerateFloors() {
        GameObject floorParent = new GameObject("floor");
        floorParent.transform.parent = instantiated.transform;
        floorParent.name = "Floors";

        for (int i = 0; i < Horizontal; i++) {
            for (int k = 0; k < Vertical; k++) {
                Vector2 tilePos = new Vector2(transform.position.x - Mathf.Ceil(((float)Horizontal / 2.0f)) + i,
                    transform.position.y + Mathf.Ceil(((float)Vertical / 2f)) - k);

                GameObject newInstance = Instantiate(FloorPrefab, tilePos, Quaternion.identity);
                newInstance.transform.parent = floorParent.transform;
                newInstance.name = "Floor";

            }
        }
    }

    public void GenerateWalls() {
        GameObject wallParent = new GameObject("Walls");
        wallParent.transform.parent = instantiated.transform;
        for (int i = 0; i < Horizontal + 2; i++) {
            for (int k = 0; k < Vertical + 2; k++) {
                GameObject prefabToSpawn;
                Vector2 spawn = new Vector2(transform.position.x - Mathf.Ceil(((float)(Horizontal + 2.0f) / 2.0f)) + i,
                    transform.position.y + Mathf.Ceil(((float)(Vertical + 2f) / 2f)) - k);
                if (k == 0 || k == Vertical + 2 - 1) {
                    prefabToSpawn = WallPrefab[0];
                    if ((i == 0 || i == Horizontal + 2 - 1) && k == 0) prefabToSpawn = WallPrefab[1];
                    else prefabToSpawn = WallPrefab[0];
                }
                else {
                    prefabToSpawn = WallPrefab[1];
                }

                if ((k != 0 || k != Vertical + 2 - 1) && (i == 0 || i == Horizontal + 2 - 1)) {
                    GameObject instantiate = Instantiate(prefabToSpawn, spawn, Quaternion.identity);
                    instantiate.name = "walls";
                    instantiate.transform.parent = wallParent.transform;
                }
                else if ((k == 0 || k == Vertical + 2 - 1)) {
                    GameObject instantiate = Instantiate(prefabToSpawn, spawn, Quaternion.identity);
                    instantiate.name = "walls";
                    instantiate.transform.parent = wallParent.transform;
                }
            }
        }
    }

    public void GenerateInnerWalls() {
        Vector2 maxPos = new Vector2(transform.position.x - Mathf.Ceil(((float)Horizontal / 2.0f)) + Horizontal,
           transform.position.y + Mathf.Ceil(((float)Vertical / 2f)) - Vertical);
        Vector2 minPos = new Vector2(transform.position.x + Mathf.Ceil(((float)Horizontal / 2.0f)) - Horizontal,
            transform.position.y - Mathf.Ceil(((float)Vertical / 2f)) + Vertical);

        GameObject randomWalls = new GameObject("Random Walls");
        randomWalls.transform.parent = instantiated.transform;
        if(InnerWalls >= (float) ((Horizontal * Vertical) / 2.0f)) {
            InnerWalls = (int) ((float)(Horizontal * Vertical) / 2.0f);
        }
        if (wallGen == WallGeneration.Random) {
            for (int i = 0; i < InnerWalls; i++) {
                Vector2 newPos;
                do {
                    newPos = RandomPosition(minPos, maxPos);
                } while (spawnPoints.Contains(newPos));
                spawnPoints.Add(newPos);
                GameObject walls = Instantiate(WallPrefab[0], newPos, Quaternion.identity);
                walls.name = "innerWalls";
                walls.transform.parent = randomWalls.transform;
            }
        }
        else if (wallGen == WallGeneration.Uniform) {
            InnerWalls = 0;
            Vector2 distance = maxPos - minPos;

            for(int i = 0; i < Mathf.Floor((float)(Horizontal - 1f) /2.0f); i++) {
                for(int k = 0; k < Mathf.Floor((float) (Vertical - 1f) /2.0f); k++) {
                    Vector2 spawnPos;
                    spawnPos.x = Mathf.Ceil((distance.x * ((i + 1) / (Horizontal/2f)) - Mathf.Ceil((float)(Horizontal + 2) /2.0f)));
                    spawnPos.y = Mathf.Floor((distance.y * ((k + 1) / (Vertical/2f)) + Mathf.Ceil((float)(Vertical + 2) / 2.0f))) ;

                    GameObject wall = Instantiate(WallPrefab[0], spawnPos, Quaternion.identity);
                    wall.transform.parent = randomWalls.transform;
                    wall.name = "insideWall";
                }
            }
        }
    }

    public Vector2 RandomPosition(Vector2 minPos, Vector2 maxPos) {
        Vector2 newPosition;
        newPosition.x = (int) Random.Range(minPos.x, maxPos.x);
        newPosition.y = (int) Random.Range(minPos.y, maxPos.y);
        return newPosition;
    }

}
