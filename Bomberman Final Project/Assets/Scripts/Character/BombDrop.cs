﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BombDrop : MonoBehaviourPunCallbacks
{
    public int bombToDrop = 1;
    int droppedBomb = 0;

    public GameObject BombPrefab;

    public int explosionSize = 1;
    
    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            if(droppedBomb < bombToDrop) {
                droppedBomb++;
                photonView.RPC("Drop", RpcTarget.All);
                //Drop();
            }
        }
    }

    [PunRPC]
    public void Drop() {
        Vector2 dropPosition = transform.position;
        dropPosition.x = Mathf.RoundToInt(dropPosition.x);
        dropPosition.y = Mathf.RoundToInt(dropPosition.y);
        //Bomb x = PhotonNetwork.Instantiate(BombPrefab.name, dropPosition, Quaternion.identity).GetComponent<Bomb>();
        Bomb x = Instantiate(BombPrefab, dropPosition, Quaternion.identity).GetComponent<Bomb>();
        x.exploded += () => {
            droppedBomb--;
            if (droppedBomb < 0) droppedBomb = 0;
        };
        x.ExplosionSize = explosionSize;
        x.owner = gameObject;
    }
}
