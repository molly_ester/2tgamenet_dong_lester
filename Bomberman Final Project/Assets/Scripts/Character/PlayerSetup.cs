﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks {
    Camera camera;
    [SerializeField] TextMeshProUGUI name;
    private void Awake() {
        camera = GetComponentInChildren<Camera>();
        name.text = photonView.Owner.NickName;

        if (!photonView.IsMine) {
            camera.enabled = false;
            GetComponent<BombDrop>().enabled = false;
            GetComponent<Movement>().enabled = false;
            camera.GetComponent<AudioListener>().enabled = false;
            //name.enabled = false;
        }
        else {
            GetComponent<Health>().DeathEvt += GameManager.instance.PlayerDeath;
            
        }
    }
}
