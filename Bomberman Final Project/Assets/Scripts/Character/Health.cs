﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;
using TMPro;

public class Health : MonoBehaviourPunCallbacks {
    
    int currentHealth;
    [SerializeField] int maxHealth;

    public int CurrentHealth => currentHealth;
    public int MaxHealth => MaxHealth;
    public Action<GameObject> DeathEvt;

    [SerializeField] Text HealthText;
    [SerializeField] TextMeshProUGUI HealthTextUnder;

    void Start() {
        HealthText.enabled = photonView.IsMine;
        HealthTextUnder.enabled = !photonView.IsMine;
        currentHealth = maxHealth;
        HealthText.text = "Health: " + currentHealth;
        HealthTextUnder.text = "Health: " + currentHealth;
        GameManager.instance.Winner += Wins;
    }

    [PunRPC]
    public void TakeDamage(int damage) {
        if (currentHealth == 0) return;
        currentHealth -= damage;
        currentHealth = Mathf.Max(CurrentHealth, 0);
        HealthText.text = "Health: " + currentHealth;
        HealthTextUnder.text = "Health: " + currentHealth;
        if (currentHealth == 0) {
            DeathEvt?.Invoke(gameObject);
        }
    }

    public void Wins() {
        if (currentHealth <= 0) return;
        int id = PhotonNetwork.LocalPlayer.ActorNumber;

        object[] data = new object[] { photonView.Owner.NickName };
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions options = new SendOptions {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.WinPlayerEvent, data, raiseEventOptions, options);
    }

}
