﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    
    Animator anim;
    public float speed = 2;

    void Start() {
        anim = GetComponent<Animator>() ?? null;
    }

    // Update is called once per frame
    void Update() {
        Move();
    }

    void Move() {
        Vector2 movement = Vector2.zero;
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        
        if(movement.x != 0) {
            movement.y = 0;
        }
        
        
        bool isMoving = movement.x != 0 || movement.y != 0;
        if (anim) {
            if (isMoving) {
                anim?.SetFloat("Horizontal", movement.x);
                anim?.SetFloat("Vertical", movement.y);
            }
            anim?.SetBool("isMoving", isMoving);
        }

        transform.Translate(movement * Time.deltaTime * speed);

        //if (!isMoving)
          //  transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));

    }

}
