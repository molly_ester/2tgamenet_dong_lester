﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BombExplosion : MonoBehaviour
{
    private void Start() {

        Destroy(gameObject, 0.5f);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        
        if (collision.gameObject.CompareTag("Player")) {
            collision.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.All, 1);
        }
    }


    IEnumerator DestroyPhoton() {
        yield return new WaitForSeconds(.5f);
        PhotonNetwork.Destroy(gameObject);
    }
}
