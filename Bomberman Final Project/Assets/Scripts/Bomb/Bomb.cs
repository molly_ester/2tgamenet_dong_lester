﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Pun;

public class Bomb : MonoBehaviourPunCallbacks
{
    Animator anim;
    int explosionSize  = 2;
    
    public int ExplosionSize { set => explosionSize = value; }

    public GameObject HorizontalExplosion;
    public GameObject VerticalExplosion;
    public GameObject CenterExplosion;

    public GameObject LeftEndExplosion;
    public GameObject RightEndExplosion;
    public GameObject UpEndExplosion;
    public GameObject DownEndExplosion;

    public Action exploded;

    public LayerMask player = 7;

    public GameObject owner;

    private void Start() {
        anim = GetComponent<Animator>();
        StartCoroutine(TimerForExplosion());
    }

    [PunRPC]
    void SetExplosion() {
        StartCoroutine(TimerForExplosion());
    }
    IEnumerator TimerForExplosion() {
        float timer = 0; 
        for(int i = 0; i <= 3; i++) {
            yield return new WaitForSeconds(1.0f);
            timer++;
            anim.SetFloat("BombTimer", timer);
        }

        Explode();
    }

    void Explode() {
        exploded?.Invoke();
        GetComponent<BoxCollider2D>().enabled = false;
        SpawnExplosion(Vector2.up);
        SpawnExplosion(Vector2.down);
        SpawnExplosion(Vector2.left);
        SpawnExplosion(Vector2.right);
        //GameObject x = PhotonNetwork.Instantiate(CenterExplosion.name, transform.position, Quaternion.identity);
        GameObject x = Instantiate(CenterExplosion, transform.position, Quaternion.identity);
        x.GetComponent<BoxCollider2D>().enabled = owner && owner.GetPhotonView().IsMine;
        GetComponent<SpriteRenderer>().sprite = null;
        Destroy(gameObject);
    }

    void SpawnExplosion(Vector2 direction) {
        for (int i = 1; i < explosionSize; i++) {
            //2
            RaycastHit2D hit;
            //3
            hit = Physics2D.Raycast((Vector2)transform.position, direction,
              i, ~player);

            ////4
            ///
            if (!hit.collider) {
                GameObject instantiated = null;
                if (i != explosionSize - 1) {

                    if (direction.x != 0)
                        instantiated = HorizontalExplosion;
                    if (direction.y != 0)
                        instantiated = VerticalExplosion;
                }
                else if (i == explosionSize - 1) {
                    if (direction == Vector2.left)
                        instantiated = LeftEndExplosion;

                    if (direction == Vector2.right)
                        instantiated = RightEndExplosion;

                    if (direction == Vector2.down)
                        instantiated = DownEndExplosion;

                    if (direction == Vector2.up)
                        instantiated = UpEndExplosion;
                }
                GameObject explosion = Instantiate(instantiated, transform.position + (Vector3)(i * direction), Quaternion.identity);
                explosion.GetComponent<BoxCollider2D>().enabled = owner && owner.GetPhotonView().IsMine;
                //GameObject explosion = PhotonNetwork.Instantiate(instantiated.name, transform.position + (Vector3)(i * direction), Quaternion.identity);
                //if (owner != null) {
                //    if (!owner.GetPhotonView().IsMine) {
                //        explosion.GetComponent<BoxCollider2D>().enabled = false;
                //    }
                //}
                //else if (owner == null) { explosion.GetComponent<BoxCollider2D>().enabled = false; }
                //Destroy(instantiated, .5f);
                //Instantiate(explosionPrefab, transform.position + (i * direction), Quaternion.identity);
                //6
            }
            else { //7
                if (hit.collider.gameObject.layer == 8) {
                    if (hit.collider.gameObject.CompareTag("Breakable")) {
                        Breakable z = hit.collider.gameObject.GetComponent<Breakable>();
                        z.ToDestroy(hit.collider.gameObject.name);
                        if (z.powerUpPrefab)
                            PhotonNetwork.Instantiate(z.powerUpPrefab.name, z.transform.position, Quaternion.identity);
                    }
                }
                else if (hit.collider.gameObject.layer == 6) {
                    Bomb b = hit.collider.gameObject.GetComponent<Bomb>();
                    StopCoroutine(b.TimerForExplosion());
                    b.Explode();
                }
                Debug.Log("mamamo");
                break;

            }
            ////8
        }
    }
    
    IEnumerator PhotonDestroyWithTimer(GameObject obj, float timer) {
        yield return new WaitForSeconds(timer);
        PhotonNetwork.Destroy(obj);
    }
}
