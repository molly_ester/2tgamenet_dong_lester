﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class PlayerListItemInitializer : MonoBehaviour {
    public enum ColorType {
        Green = 1, Blue = 2, Violet = 3, Yellow = 4
    }
    [Header("UI References")]
    public TextMeshProUGUI PlayerNameText;
    public Button PlayerReadyButton;
    public Image PlayerReadyImage;
    public Image playerColor;
     
    bool isPlayerReady = false;
    public ColorType colorType = ColorType.Green;
    public GameObject chooseButton;

    bool isOwner;

    public void Initialize(int playerId, string playerName) {
        PlayerNameText.text = playerName;
        SetColor();
        isOwner = PhotonNetwork.LocalPlayer.ActorNumber == playerId;
        if (PhotonNetwork.LocalPlayer.ActorNumber != playerId) {
            PlayerReadyButton.gameObject.SetActive(false);
            chooseButton.SetActive(false);
        }
        else {
            ExitGames.Client.Photon.Hashtable initializeProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_READY, isPlayerReady } };
            PhotonNetwork.LocalPlayer.SetCustomProperties(initializeProperties);
            
            PlayerReadyButton.onClick.AddListener(() => {
                isPlayerReady = !isPlayerReady;
                SetPlayerReady(isPlayerReady);

                ExitGames.Client.Photon.Hashtable newProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_READY, isPlayerReady } };
                PhotonNetwork.LocalPlayer.SetCustomProperties(newProperties);

            });
        }
    }

    public void SetPlayerReady(bool playerReady) {
        PlayerReadyImage.enabled = playerReady;
        chooseButton.SetActive(!playerReady && isOwner);
        if (playerReady) {
            PlayerReadyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Ready!";       
        }
        else {
            PlayerReadyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Ready?";
        }
    }

    public void SetColor() {
        switch (colorType) {
            case (ColorType)1:
                playerColor.color = Color.green;
                break;
            case (ColorType)2:
                playerColor.color = Color.blue;
                break;
            case (ColorType)3:
                playerColor.color = Color.magenta;
                break;
            case (ColorType)4:
                playerColor.color = Color.yellow;
                break;
        }
        ExitGames.Client.Photon.Hashtable playerProperty = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_SELECTION_NUMBER, (int)colorType } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerProperty);
    }

    public void NextColor() {
        colorType++;
        if ((int) colorType > 4) {
            colorType = (ColorType) 1;
        }
        SetColor();
    }

    public void PreviousColor() {
        colorType--;
        if((int) colorType < 1) {
            colorType = (ColorType) 4;
        }
        SetColor((int)colorType);
    }

    public void SetColor(int color) {
        switch (color) {
            case 1:
                playerColor.color = Color.green;
                break;
            case 2:
                playerColor.color = Color.blue;
                break;
            case 3:
                playerColor.color = Color.magenta;
                break;
            case 4:
                playerColor.color = Color.yellow;
                break;
        }
    }

    public void Dbug() {
        Debug.Log("HOOY");
    }


}
