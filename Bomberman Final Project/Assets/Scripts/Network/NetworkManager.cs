﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using Photon.Realtime;

public class NetworkManager : MonoBehaviourPunCallbacks {

    [Header("Login UI")]
    public GameObject LoginUIPanel;
    public TMP_InputField PlayerNameInput;

    [Header("Connecting Info Panel")]
    public GameObject ConnectingInfoUIPanel;

    [Header("Creating Room Info Panel")]
    public GameObject CreatingRoomInfoUIPanel;

    [Header("GameOptions  Panel")]
    public GameObject GameOptionsUIPanel;

    [Header("Create Room Panel")]
    public GameObject CreateRoomUIPanel;
    public TMP_InputField RoomNameInputField;

    [Header("Show Room List Panel")]
    public GameObject showRoomListPanel;

    [Header("Inside Room Panel")]
    public GameObject InsideRoomUIPanel;
    public TextMeshProUGUI RoomInfoText;
    public TextMeshProUGUI RoomSizeText;
    public GameObject PlayerListPrefab;
    public GameObject PlayerListParent;
    public GameObject StartGameButton;

    [Header("Room List Panel")]
    public GameObject roomListPanel;
    public GameObject roomItemPrefab;
    public GameObject roomListParent;

    Dictionary<string, RoomInfo> cachedRoomList;
    Dictionary<string, GameObject> roomListGameObject;

    public Dictionary<int, GameObject> playerListGameObjects;

    void Start() {
        ActivatePanel(LoginUIPanel.name);
        cachedRoomList = new Dictionary<string, RoomInfo>();
        roomListGameObject = new Dictionary<string, GameObject>();
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    #region UI callbacks
    public void OnLoginButtonClicked() {
        string playerName = PlayerNameInput.text;

        if (!string.IsNullOrEmpty(playerName)) {
            ActivatePanel(ConnectingInfoUIPanel.name);

            if (!PhotonNetwork.IsConnected) {
                PhotonNetwork.LocalPlayer.NickName = playerName;
                PhotonNetwork.ConnectUsingSettings();
            }
        }
        else {
            Debug.Log("PlayerName is invalid!");
        }
    }

    public void OnCancelButtonClicked() {
        ActivatePanel(GameOptionsUIPanel.name);
    }

    public void OnBackButtonClicked() {
        ActivatePanel(GameOptionsUIPanel.name);
    }

    public void OnCreateRoomButtonClicked() {
        ActivatePanel(CreatingRoomInfoUIPanel.name);
        string roomName = RoomNameInputField.text;

        if (string.IsNullOrEmpty(roomName)) {
            roomName = "Room " + Random.Range(1000, 10000);
        }
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 4;

        //    //gameModes
        //    //rc = racing
        //    //dr = deathrace
        //    ExitGames.Client.Photon.Hashtable customProperties = new ExitGames.Client.Photon.Hashtable() { { "gm", GameMode } };

        //    roomOptions.CustomRoomPropertiesForLobby = roomPropertiesInLobby;
        //    roomOptions.CustomRoomProperties = customProperties;
        PhotonNetwork.CreateRoom(roomName, roomOptions);

    }

    public void OnJoinRandomRoomClicked() {
        PhotonNetwork.JoinRandomRoom();
    }

    public void OnCreateButtonClicked() {
        ActivatePanel(CreateRoomUIPanel.name);
    }

    public void OnLeaveGameButtonClicked() {
        PhotonNetwork.LeaveRoom();
        //ActivatePanel(Game
    }

    public void OnStartGameButtonClicked() {
        //Debug.Log(PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("gm") + " and " + PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"));
        //Debug.Log(PhotonNetwork.CurrentRoom.CustomProperties);
        //if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("gm")) {
        //    if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc")) {
        //        PhotonNetwork.LoadLevel("RacingScene");

        //    }
        //    else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr")) {
        //        PhotonNetwork.LoadLevel("DeathRaceScene");
        //    }
        //}

        PhotonNetwork.LoadLevel("GameScene");
    }

    public override void OnLeftLobby() {
        ClearRoomListGameObjects();
        cachedRoomList.Clear();
    }


    public override void OnRoomListUpdate(List<RoomInfo> roomList) {
        ClearRoomListGameObjects();

        //startGameButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);
        foreach (RoomInfo info in roomList) {
            Debug.Log(info.Name);
            if (!info.IsOpen || !info.IsVisible || info.RemovedFromList) {
                if (cachedRoomList.ContainsKey(info.Name)) {
                    cachedRoomList.Remove(info.Name);
                }
            }
            else {
                if (cachedRoomList.ContainsKey(info.Name)) {
                    cachedRoomList[info.Name] = info;
                }
                else {
                    cachedRoomList.Add(info.Name, info);
                }
            }
        }

        foreach (RoomInfo info in cachedRoomList.Values) {
            GameObject listItem = Instantiate(roomItemPrefab);
            listItem.transform.SetParent(roomListParent.transform);
            listItem.transform.localScale = Vector3.one;

            listItem.transform.Find("RoomNameText").GetComponent<Text>().text = info.Name;
            listItem.transform.Find("RoomPlayersText").GetComponent<Text>().text = "Player count: " + info.PlayerCount + "/" + info.MaxPlayers;
            listItem.transform.Find("JoinRoomButton").GetComponent<Button>().onClick.AddListener(delegate { OnJoinRoomClicked(info.Name); });

            roomListGameObject.Add(info.Name, listItem);
        }
    }

    public void OnShowRoomListButtonClicked() {
        if (!PhotonNetwork.InLobby) {
            PhotonNetwork.JoinLobby();
        }
        ActivatePanel(showRoomListPanel.name);
    }


    #endregion

    #region Public Methods
    public void ActivatePanel(string panelNameToBeActivated) {
        LoginUIPanel.SetActive(LoginUIPanel.name.Equals(panelNameToBeActivated));
        ConnectingInfoUIPanel.SetActive(ConnectingInfoUIPanel.name.Equals(panelNameToBeActivated));
        CreatingRoomInfoUIPanel.SetActive(CreatingRoomInfoUIPanel.name.Equals(panelNameToBeActivated));
        CreateRoomUIPanel.SetActive(CreateRoomUIPanel.name.Equals(panelNameToBeActivated));
        GameOptionsUIPanel.SetActive(GameOptionsUIPanel.name.Equals(panelNameToBeActivated));
        InsideRoomUIPanel.SetActive(InsideRoomUIPanel.name.Equals(panelNameToBeActivated));
        showRoomListPanel.SetActive(showRoomListPanel.name.Equals(panelNameToBeActivated));
    }
    #endregion

    #region Photon Callbacks
    public override void OnConnected() {
        Debug.Log("Connected to Internet");
    }

    public override void OnConnectedToMaster() {
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " is connected to Photon");
        ActivatePanel(GameOptionsUIPanel.name);
    }

    public override void OnCreatedRoom() {
        Debug.Log(PhotonNetwork.CurrentRoom + " has been created!");
    }

    public override void OnJoinedRoom() {

        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " has joined the room " + PhotonNetwork.CurrentRoom.Name);

        ActivatePanel(InsideRoomUIPanel.name);

        RoomInfoText.text = "Room name: " + PhotonNetwork.CurrentRoom.Name;
        RoomSizeText.text = "Players: " + PhotonNetwork.CurrentRoom.PlayerCount + " / " + PhotonNetwork.CurrentRoom.MaxPlayers;

        if (playerListGameObjects == null) {
            playerListGameObjects = new Dictionary<int, GameObject>();
        }

        foreach (Player player in PhotonNetwork.PlayerList) {
            GameObject playerListItem = Instantiate(PlayerListPrefab);
            playerListItem.transform.SetParent(PlayerListParent.transform);
            playerListItem.transform.localScale = Vector3.one;

            playerListItem.GetComponent<PlayerListItemInitializer>().Initialize(player.ActorNumber, player.NickName);

            object isPlayerReady;
            if (player.CustomProperties.TryGetValue(Constants.PLAYER_READY, out isPlayerReady)) {
                playerListItem.GetComponent<PlayerListItemInitializer>().SetPlayerReady((bool)isPlayerReady);
            }
            playerListGameObjects.Add(player.ActorNumber, playerListItem);
        }

        StartGameButton.SetActive(false);
    }

    public override void OnJoinRandomFailed(short returnCode, string message) {
        Debug.Log(message);
        ActivatePanel(CreatingRoomInfoUIPanel.name);
        string roomName = RoomNameInputField.text;

        if (string.IsNullOrEmpty(roomName)) {
            roomName = "Room " + Random.Range(1000, 10000);
        }
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 4;

        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer) {
        GameObject playerListItem = Instantiate(PlayerListPrefab);
        playerListItem.transform.SetParent(PlayerListParent.transform);
        playerListItem.transform.localScale = Vector3.one;

        playerListItem.GetComponent<PlayerListItemInitializer>().Initialize(newPlayer.ActorNumber, newPlayer.NickName);

        playerListGameObjects.Add(newPlayer.ActorNumber, playerListItem);
        RoomInfoText.text = "Room name: " + PhotonNetwork.CurrentRoom.Name;
        RoomSizeText.text = "Players: " + PhotonNetwork.CurrentRoom.PlayerCount + " / " + PhotonNetwork.CurrentRoom.MaxPlayers;

        StartGameButton.SetActive(CheckAllPlayerReady());
    }

    public override void OnLeftRoom() {
        ActivatePanel(GameOptionsUIPanel.name);

        foreach (GameObject playerListGameObject in playerListGameObjects.Values) {
            Destroy(playerListGameObject);
        }

        playerListGameObjects.Clear();
        playerListGameObjects = null;
    }
    public override void OnPlayerLeftRoom(Player otherPlayer) {
        Destroy(playerListGameObjects[otherPlayer.ActorNumber]);
        playerListGameObjects.Remove(otherPlayer.ActorNumber);
        RoomInfoText.text = "Room name: " + PhotonNetwork.CurrentRoom.Name;
        RoomSizeText.text = "Players: " + PhotonNetwork.CurrentRoom.PlayerCount + " / " + PhotonNetwork.CurrentRoom.MaxPlayers;
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps) {
        GameObject playerlistGameObject;
        if (playerListGameObjects.TryGetValue(targetPlayer.ActorNumber, out playerlistGameObject)) {
            object isPlayerReady;
            PlayerListItemInitializer init = playerlistGameObject.GetComponent<PlayerListItemInitializer>();
            if (changedProps.TryGetValue(Constants.PLAYER_READY, out isPlayerReady)) {
                init.SetPlayerReady((bool)isPlayerReady);
            }
            if(changedProps.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out object color)) {
                init.SetColor( (int) color);
            }
        }

        StartGameButton.SetActive(CheckAllPlayerReady());

    }

    public override void OnMasterClientSwitched(Player newMasterClient) {
        if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber) {
            StartGameButton.SetActive(CheckAllPlayerReady());
        }
    }
    #endregion

    #region Private Methods
    bool CheckAllPlayerReady() {
        if (!PhotonNetwork.IsMasterClient) {
            return false;
        }

        foreach (Player p in PhotonNetwork.PlayerList) {
            object isPlayerReady;

            if (p.CustomProperties.TryGetValue(Constants.PLAYER_READY, out isPlayerReady)) {
                if (!(bool)isPlayerReady)
                    return false;

            }
            else return false;

        }
        return true;
    }

    private void ClearRoomListGameObjects() {
        foreach (var item in roomListGameObject.Values) {
            Destroy(item);
        }

        roomListGameObject.Clear();
    }

    void OnJoinRoomClicked(string roomName) {
        if (PhotonNetwork.InLobby) {
            PhotonNetwork.LeaveLobby();
        }
        PhotonNetwork.JoinRoom(roomName);
    }

    #endregion
}
