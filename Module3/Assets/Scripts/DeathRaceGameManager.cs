﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

using UnityEngine.UI;

public class DeathRaceGameManager : MonoBehaviour
{
    public GameObject[] VehiclePrefabs;
    public Transform[] StartingPositions;

    public static DeathRaceGameManager instance;

    public Text CountdownText;
    public Text[] EliminatedText;

    private void Awake() {
        if(instance == null) {
            instance = this;
        }
        if(instance != this) {
            Destroy(instance);
        }
    }

    private void Start() {
        if (PhotonNetwork.IsConnectedAndReady) {
            object playerSelectionNumber;
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber)) {
                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = StartingPositions[actorNumber - 1].position;
                PhotonNetwork.Instantiate(VehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            }
        }

        foreach(var go in EliminatedText) {
            go.gameObject.SetActive(false);
        }
    }
}
