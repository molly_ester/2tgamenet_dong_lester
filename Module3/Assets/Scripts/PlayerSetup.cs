﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;
using UnityEngine.UI;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject gun;
    public GameObject canvas;

    public Func<string, bool> CustomPropertiesValue = x => PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(x);
    void Awake() {
        camera = transform.Find("Camera").GetComponent<Camera>();
        canvas = GetComponentInChildren<Canvas>().gameObject;
        if (CustomPropertiesValue("rc") || CustomPropertiesValue("dr")) {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = CustomPropertiesValue("rc") && photonView.IsMine;
        }
        canvas.SetActive(CustomPropertiesValue("dr"));

        if (gun != null) {
            gun?.SetActive(CustomPropertiesValue("dr"));
            GetComponent<Shooting>().enabled = CustomPropertiesValue("dr") && photonView.IsMine;
        }
    }
}
