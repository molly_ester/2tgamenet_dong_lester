﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;
public enum RaiseEventsCode {
    WhoFinishedEventCode = 0,
    WhoDiedEventCode = 1, WhoWonEventCode = 2
}

public class LapController : MonoBehaviourPunCallbacks
{
    public List<GameObject> lapTriggers = new List<GameObject>();
    

    int finishOrder = 0;

    void OnEnable() {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    void OnDisable() {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent) {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoFinishedEventCode) {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfFinshedPlayer = (string) data[0];
            finishOrder = (int)data[1];
            int viewId = (int)data[2];

            Debug.Log(nickNameOfFinshedPlayer + " " + finishOrder);
            GameObject orderUiText = RacingGameManager.instance.finisherTextUI[finishOrder - 1];
            orderUiText.SetActive(true);

            Text text = orderUiText.GetComponent<Text>();
            if (viewId == photonView.ViewID) {
                text.text = finishOrder + " " + nickNameOfFinshedPlayer + "(YOU)";
                text.color = Color.red;
            }
            else {
                text.text = finishOrder + " " + nickNameOfFinshedPlayer;
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject go in RacingGameManager.instance.lapTrigger) {
            lapTriggers.Add(go);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (lapTriggers.Contains(other.gameObject)) {
            int indexOfTrigger = lapTriggers.IndexOf(other.gameObject);
            lapTriggers[indexOfTrigger].SetActive(false);
        }

        if(other.gameObject.tag == "FinishTrigger") {
            GameFinish();
        }
    }

    public void GameFinish() {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovement>().enabled = false ;
        finishOrder++;
        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;
        object[] data = new object[] { nickName, finishOrder, viewId };
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions options = new SendOptions {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions, options);
    }
}
