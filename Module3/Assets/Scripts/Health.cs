﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Health : MonoBehaviourPunCallbacks {

    public int CurrentHealth;
    public int MaxHealth;

    public Image healthBar;

    public int InGameAlive;
    bool isDead;

    void OnEnable() {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    void OnDisable() {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent) {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoDiedEventCode) {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfFinshedPlayer = (string)data[0];
            InGameAlive = (int)data[1];
            int viewId = (int)data[2];

            Debug.Log(nickNameOfFinshedPlayer + " " + InGameAlive);
            Text orderUiText = DeathRaceGameManager.instance.EliminatedText[InGameAlive];
            orderUiText.gameObject.SetActive(true);
            orderUiText.text = InGameAlive + 1 +  ": " + nickNameOfFinshedPlayer + " is Eliminated!";
            
            if(CurrentHealth > 0 && InGameAlive == 1) {
                Text x = DeathRaceGameManager.instance.EliminatedText[InGameAlive - 1];
                x.gameObject.SetActive(true);

                x.text = InGameAlive + ": " + photonView.Owner.NickName + " wins ";
            }
        }
    }

    private void Start() {
        InGameAlive = PhotonNetwork.CurrentRoom.PlayerCount;
    }
    [PunRPC]
    public void TakeDamage(int damage) {
        CurrentHealth -= damage;
        CurrentHealth = Mathf.Max(0, CurrentHealth);

        healthBar.fillAmount = (float)CurrentHealth / (float)MaxHealth;

        if(CurrentHealth <= 0 && !isDead) {
            Die();
            isDead = true;
        }
    }

    public void Die() {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovement>().enabled = false;
        GetComponent<Shooting>().enabled = false;
        InGameAlive--;
        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;
        object[] data = new object[] { nickName, InGameAlive, viewId };
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions options = new SendOptions {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoDiedEventCode, data, raiseEventOptions, options);
        
    }
}
