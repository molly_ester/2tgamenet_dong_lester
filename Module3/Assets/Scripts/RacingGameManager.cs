﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class RacingGameManager : MonoBehaviour {
    public GameObject[] vehiclePrefabs;
    public Transform[] startingPositions;

    public static RacingGameManager instance = null;

    public Text timeText;
    public GameObject[] finisherTextUI;
    public List<GameObject> lapTrigger = new List<GameObject>();

    private void Awake() {
        if (!instance) {
            instance = this;
        }
        else if(instance != this) {
            Destroy(gameObject);
        }
    }
    void Start() {
        if (PhotonNetwork.IsConnectedAndReady) {
            object playerSelectionNumber;   
            if(PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber)) {
                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
                PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            }
        }

        foreach(GameObject go in finisherTextUI) {
            go.SetActive(false);
        }
    }
}
