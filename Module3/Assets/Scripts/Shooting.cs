﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    Camera camera;

    public bool isControlEnabled = false;
    public enum FiringType {
        ray,
        projectile
    };

    public FiringType type;
    public GameObject projectilePrefab;
    public Transform spawnPosition;
    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponentInChildren<Camera>();
    }

    // Update is called once per frame
    void Update() {
        if (!isControlEnabled) return;
        if (Input.GetMouseButtonDown(0)) {
            if(type == FiringType.ray) {
                Ray ray = camera.ViewportPointToRay(new Vector3(.5f, .6f));
                if(Physics.Raycast(ray, out RaycastHit hit, 200)) {
                    Health health;
                    if ((health = hit.collider.gameObject.GetComponent<Health>()) && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine) {
                        hit.collider.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 1);
                    }
                }
            }
            else if(type == FiringType.projectile) {
                //photonView.RPC("Spawn", RpcTarget.All);
                PhotonNetwork.Instantiate(projectilePrefab.name, spawnPosition.position, transform.rotation);
            }
        }
    }

    [PunRPC]
    public void Spawn() {
        Instantiate(projectilePrefab, spawnPosition.position, transform.rotation);
    }
}
