﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Projectile : MonoBehaviourPunCallbacks
{
    bool hit;
    // Update is called once per frame
    void Update()
    {
        //photonView.RPC("MoveContinuously", RpcTarget.All);
        MoveContinuously();
    }
    public void MoveContinuously() {
        transform.Translate(Vector3.forward);
    }



    private void OnCollisionEnter(Collision collision) {
        GetComponent<BoxCollider>().enabled = false;
        Health health;
        if ((health = collision.gameObject.GetComponent<Health>()) && !hit) {
            health.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 1);
            PhotonNetwork.Destroy(transform.parent.gameObject);
            //Destroy(transform.parent.gameObject);
        }
    }

}
