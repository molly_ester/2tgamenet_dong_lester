﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSelection : MonoBehaviour
{
    public GameObject[] SelectablePlayers;
    int playerSelectionNumber;
    // Start is called before the first frame update
    void Start() {
        playerSelectionNumber = 0;
        ActivatePlayer(playerSelectionNumber);
    }

    public void GoToNextPlayer() {
        playerSelectionNumber++;
        if (playerSelectionNumber >= SelectablePlayers.Length) playerSelectionNumber = 0;
        ActivatePlayer(playerSelectionNumber);

    }

    private void ActivatePlayer(int x) {
        foreach(var go in SelectablePlayers) {
            go.SetActive(false);
        }

        SelectablePlayers[x].SetActive(true);
        //setting the player Selection for the vehicle
        ExitGames.Client.Photon.Hashtable playerSelectionProperties = new ExitGames.Client.Photon.Hashtable() {
            {Constants.PLAYER_SELECTION_NUMBER, playerSelectionNumber } 
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerSelectionProperties);

    }
    public void GoToPreviousPlayer() {
        playerSelectionNumber--;
        if (playerSelectionNumber < 0) playerSelectionNumber = SelectablePlayers.Length - 1;
        ActivatePlayer(playerSelectionNumber);
    }
}
